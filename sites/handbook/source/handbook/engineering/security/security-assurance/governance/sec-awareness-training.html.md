---
layout: handbook-page-toc
title: "Security Awareness Training Program"
description: "Security Awareness Training Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Security Awareness Training Program

The GitLab security awareness training program provides ongoing training to GitLab team members that enhances knowledge and identification of cybersecurity threats, vulnerabilities, and attacks. Security Awareness Training is provided by ProofPoint, GitLab's third party provider, and will help satisfy external regulatory requirements and bolster customer assurance.

## Security Awareness Training Campaigns

Security Awareness Training is an integral part of GitLab's overall security strategy. The Security Governance team utilizes ProofPoint to deliver training campaigns designed to provide GitLab team members with the information they need to protect themselves and GitLab from loss or harm, highlight their role in securing GitLab on a daily basis, and empower them to make the right decisions with security best-practices. 

## When will security awareness training occur?

General Security Awareness Training will occur no less than annually in the second quarter of each fiscal year. Additional role-based awareness training may be assigned as needed. Prior to the security awareness training taking place, a general notification to the GitLab organization will be posted to the `#whats-happening-at-gitlab` Slack channel.

## Who will receive the annual security awareness training?

All GitLab team members, contractors and others with access to production data will be included in the campaigns with the exception of any individuals on extended leave at the time the campaign is launched. 

For annual General Security Awareness Training, all team members hired prior to June 1 of the current year will receive an email from our training vendor, ProofPoint. GitLab team members hired after June 1 of the current year will have undergone this training as part of their onboarding and therefore will not be required to take the annual security awareness training until the following year.

## How long with the training take? 

The security awareness training has been limited to 30 minutes in an effort to find the best return of security investment from team-member's time.  

## What will be covered in the training?

There will be a GitLab-specific introduction module followed by industry-standard training via ProofPoint.  There will be a short quiz to identify what you have learned.

* Special topics covered:
    * [Suspected phishing](https://about.gitlab.com/handbook/security/#phishing-tests)
    * [Acceptable Use](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/)
    * [Device Lost or Stolen?!](https://about.gitlab.com/handbook/security/#panic-email)
        * email: panic@gitlab.com
    * [Data Classification](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html)
    * [No Red Data on Unapproved Locations](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/#security-and-proprietary-information)

## What happens if training is not completed?

The training will be available for 30 days.  If the training is not completed, Security Assurance will send weekly reminder notifications requesting completion of the training.  
If required, we will communicate incomplete assigned trainings to managers for assistance with completion.  Demonstration of a completed training supports compliance with the Security Awareness Training program and will strengthen our regulatory requirements.

### Security Awareness Training Metrics

The Security Governance team will track the annual security awareness training completion rate in the GitLab ZenGRC instance.  Once the training campaign has completed, the Security Governance team will provide results in the [Security Awareness Training Program](https://gitlab.com/gitlab-com/gl-security/security-assurance/governance/security-awareness-training project.

### Questions and Answers

*Why are we using an external vendor?*

* While GitLab's learning platform, EdCast, is being built out it doesn't yet have the features required to support security training requirements.  The decision was made to utilize an external solution that would be able to provide a robust and consistent process that we could rely on and a seamless integration when we are ready to move all GitLab security awareness training content into EdCast as soon as the platform is able to support this training program.  We are now collaborating with ProofPoint to run our FY23 Security Awareness Training campaigns.

*How will I access training?*

* All users will have the ProofPoint tile added to their Okta account dashboard.  We are working diligently to have ProofPoint managed via Single Sign-On, but in the mean time, team members will need to access ProofPoint via this [url](https://gitlab.ws01-securityeducation.com/). if you haven't logged in before, please select 'Forget Password' to log in.  Okta then caches the credentials for subsequent logins.

*Why was I chosen?*

* All GitLab team members, contractors and anyone with access to production data will be required to complete our security awareness trainings whether it be during New Hire Orientation or annually. 

*I just took New Hire training, why do I have to take it again?*

* We realize that recently onboarded team members have gone through the new hire security training, but there is additional content in this general security awareness training that we think is valuable to everyone, so anyone hired prior to June 1, will be required to complete the training.

*I don't want to be included, how do I remove myself?*

* All GitLab team members have the responsibility to help keep themselves, team members, the company and the customer secure; all are required to complete assigned training.

*Will I be publicly shamed?*

* No, we will never post or create metrics which will associate a team member success or failure with a training.  If we release metrics company-wide, we will generate non-identifying metrics to be shared internally and public-facing.

*How can I provide Feedback on my experience?*

* Please feel free to add any feedback, comments, concerns on this [feedback issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/governance/security-awareness-training/security-awareness-training-program/-/issues/3).

### Additional Questions, Comments, Concerns?

Please reach out to the [Security Compliance team!](/handbook/engineering/security/security-assurance/security-compliance/compliance.html)
